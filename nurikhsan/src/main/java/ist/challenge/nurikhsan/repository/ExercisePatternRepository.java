package ist.challenge.nurikhsan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ist.challenge.nurikhsan.model.ExercisePattern;

public interface ExercisePatternRepository extends JpaRepository<ExercisePattern, Integer> {
    List<ExercisePattern> findAllByExerciseId(Integer id);
}
