package ist.challenge.nurikhsan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ist.challenge.nurikhsan.model.Exercise;

public interface ExerciseRepository extends JpaRepository<Exercise, Integer> {

    List<Exercise> findAllByOrderByIdDesc();

}
