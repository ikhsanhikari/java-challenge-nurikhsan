package ist.challenge.nurikhsan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ist.challenge.nurikhsan.model.ExerciseAnswer;
import ist.challenge.nurikhsan.payload.ListExerciseResponse;



public interface ExerciseAnswerRepository extends JpaRepository<ExerciseAnswer, Long> {
    List<ExerciseAnswer> findAllByGenerateIdOrderByNoIndex(String generateId);

    @Query(value = "select new ist.challenge.nurikhsan.payload.ListExerciseResponse(e.exerciseName,ea.generateId,e.description,COUNT(ea),ea.createdBy,ea.creationDate) from ExerciseAnswer ea join Exercise e on ea.exerciseId=e.id group by ea.generateId order by ea.creationDate desc")
    List<ListExerciseResponse> findGroupByGenerateId();

}
