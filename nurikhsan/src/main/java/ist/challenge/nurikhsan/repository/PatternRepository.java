package ist.challenge.nurikhsan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ist.challenge.nurikhsan.model.Pattern;

public interface PatternRepository extends JpaRepository<Pattern, Integer> {
    public List<Pattern> findAllByOrderByIdDesc();

}
