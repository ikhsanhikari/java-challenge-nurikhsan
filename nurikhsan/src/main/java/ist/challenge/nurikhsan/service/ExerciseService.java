package ist.challenge.nurikhsan.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ist.challenge.nurikhsan.feign.CLientApi;
import ist.challenge.nurikhsan.feign.CodexRequest;
import ist.challenge.nurikhsan.model.Exercise;
import ist.challenge.nurikhsan.model.ExerciseAnswer;
import ist.challenge.nurikhsan.model.ExercisePattern;
import ist.challenge.nurikhsan.payload.ApiResponse;
import ist.challenge.nurikhsan.payload.ApiResponseData;
import ist.challenge.nurikhsan.payload.ExerciseAnswerResponse;
import ist.challenge.nurikhsan.payload.ExerciseForGenerate;
import ist.challenge.nurikhsan.payload.ExerciseRequest;
import ist.challenge.nurikhsan.payload.GenerateExerciseResponse;
import ist.challenge.nurikhsan.payload.PatternResultResponse;
import ist.challenge.nurikhsan.repository.ExerciseAnswerRepository;
import ist.challenge.nurikhsan.repository.ExercisePatternRepository;
import ist.challenge.nurikhsan.repository.ExerciseRepository;
import ist.challenge.nurikhsan.repository.PatternRepository;
import ist.challenge.nurikhsan.util.GeneralUtil;
import ist.challenge.nurikhsan.util.StaticValue;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Slf4j
public class ExerciseService {

        private final ExercisePatternRepository exercisePatternRepository;

        private final ExerciseRepository exerciseRepository;

        private final PatternRepository patternRepository;

        private final CLientApi cLientApi;

        private final ExerciseAnswerRepository exerciseAnswerRepository;

        @Value("${app.language}")
        public String language;

        /**
         * @return
         */
        public ResponseEntity generateExerciseFromPattern(Integer id) {

                List<ExerciseForGenerate> exerciseForGenerates = getAllByExercise(id);
                UUID randomUUID = UUID.randomUUID();
                AtomicInteger index = new AtomicInteger();
                List<GenerateExerciseResponse> result = exerciseForGenerates.stream().map(item -> {
                        return processGeneratePattern(item, randomUUID);
                })
                                .flatMap(List::stream)
                                .map(item -> {
                                        cekAnswer(item, randomUUID, index.incrementAndGet());
                                        return item;
                                })
                                .collect(Collectors.toList());

                return ResponseEntity.ok(new ApiResponseData<>(result));
        }

        private List<GenerateExerciseResponse> processGeneratePattern(ExerciseForGenerate item, UUID randomUUID) {
                String result = "";
                List<PatternResultResponse> patternResult = new ArrayList<>();
                for (int i = 0; i < item.getAmount(); i++) {

                        result = item.getPattern();
                        result = GeneralUtil.replace(result, StaticValue.VALUE);
                        result = GeneralUtil.replace(result, StaticValue.COMPARE);
                        result = GeneralUtil.replace(result, StaticValue.OPERATOR);
                        result = GeneralUtil.replace(result, StaticValue.OUTPUT);
                        result = GeneralUtil.replace(result, StaticValue.LOGIC);

                        patternResult.add(PatternResultResponse.builder()
                                        .patternResult(result)
                                        .build());
                }

                List<GenerateExerciseResponse> resultResponse = patternResult.stream().map(i -> {
                        return GenerateExerciseResponse.builder()
                                        .pattern(i.getPatternResult())
                                        .exerciseId(item.getExerciseId())
                                        .build();
                }).collect(Collectors.toList());

                return resultResponse;
        }

        @Transactional
        public ResponseEntity createExercise(ExerciseRequest exerciseRequest) {
                Exercise saveExercise = exerciseRepository.save(
                                Exercise.builder()
                                                .exerciseName(exerciseRequest.getExerciseName())
                                                .description(exerciseRequest.getDescription())
                                                .build());
                exerciseRequest.getPatterns().forEach((item) -> {
                        exercisePatternRepository.save(
                                        ExercisePattern.builder()
                                                        .amount(item.getAmount())
                                                        .exercise(saveExercise)
                                                        .pattern(patternRepository.findById(item.getPatternId()).get())
                                                        .build());
                });
                return ResponseEntity.ok(new ApiResponse("exercise created!"));
        }

        public List<ExerciseForGenerate> getAllByExercise(Integer id) {
                List<ExerciseForGenerate> result = exercisePatternRepository.findAllByExerciseId(id).stream()
                                .map(item -> {
                                        return ExerciseForGenerate.builder()
                                                        .amount(item.getAmount())
                                                        .pattern(item.getPattern().getPattern())
                                                        .exerciseId(item.getExercise().getId())
                                                        .build();
                                }).collect(Collectors.toList());
                return result;
        }

        public ResponseEntity getAllExercise() {
                List<Exercise> exercises = exerciseRepository.findAllByOrderByIdDesc().stream()
                                .collect(Collectors.toList());
                return ResponseEntity.ok(new ApiResponseData<>(exercises));
        }

        @Async
        public void cekAnswer(GenerateExerciseResponse data, UUID randomUUID, int i) {
                log.info("Start async to check an answer");
                String code = data.getPattern();
                CompletableFuture.supplyAsync(() -> {
                        try {
                                Map<String, String> compile = cLientApi.compile(
                                                CodexRequest.builder()
                                                                .code(code)
                                                                .language(language)
                                                                .build());
                                compile.put("code", code);

                                return compile;
                        } catch (Exception e) {
                                e.printStackTrace();
                                throw new RuntimeException("failed to compile");
                        }

                }).thenAccept((item) -> {
                        log.info("start saving data");
                        try {
                                exerciseAnswerRepository.save(ExerciseAnswer.builder()
                                                .answer(item.get("output"))
                                                .code(code)
                                                .generateId(randomUUID.toString())
                                                .noIndex(i)
                                                .exerciseId(data.getExerciseId())
                                                .build());
                                log.info("Finish Compile for getting an answer");
                                log.info("output :{}",
                                                item.get("output") == null ? item.get("error") : item.get("output"));
                        } catch (Exception e) {
                                // TODO: handle exception
                                e.printStackTrace();
                        }

                });

        }

        public ResponseEntity getExerciseAnswerByGenerateId(String generateId) {

                List<ExerciseAnswer> result = exerciseAnswerRepository.findAllByGenerateIdOrderByNoIndex(generateId);

                List<ExerciseAnswerResponse> res = result.stream()
                                .map(item -> ExerciseAnswerResponse.builder()
                                                .noIndex(item.getNoIndex())
                                                .question(item.getCode())
                                                .generateId(item.getGenerateId())
                                                .build())
                                .collect(Collectors.toList());
                return ResponseEntity.ok(new ApiResponseData<>(res));
        }

}
