package ist.challenge.nurikhsan.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ist.challenge.nurikhsan.feign.CLientApi;
import ist.challenge.nurikhsan.feign.CodexRequest;
import ist.challenge.nurikhsan.model.Pattern;
import ist.challenge.nurikhsan.payload.ApiResponse;
import ist.challenge.nurikhsan.payload.ApiResponseData;
import ist.challenge.nurikhsan.payload.CreatePatternResponse;
import ist.challenge.nurikhsan.payload.PatternRequest;
import ist.challenge.nurikhsan.repository.ExercisePatternRepository;
import ist.challenge.nurikhsan.repository.PatternRepository;
import ist.challenge.nurikhsan.util.GeneralUtil;
import ist.challenge.nurikhsan.util.StaticValue;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PatternService {

    private final PatternRepository patternRepository;
    private final ExercisePatternRepository exercisePatternRepository;
    private final CLientApi cLientApi;

    @Value("${app.language}")
    public String language;

    private String modifyCode(String code) {
        String result = code;
        result = GeneralUtil.replace(result, StaticValue.VALUE);
        result = GeneralUtil.replace(result, StaticValue.COMPARE);
        result = GeneralUtil.replace(result, StaticValue.OPERATOR);
        result = GeneralUtil.replace(result, StaticValue.OUTPUT);
        result = GeneralUtil.replace(result, StaticValue.LOGIC);
        return result;
    }

    public ResponseEntity createPattern(PatternRequest patternRequest) {
        patternRepository.save(
                Pattern.builder()
                        .pattern(patternRequest.getPattern())
                        .courseLevel(patternRequest.getCourseLevel())
                        .courseType(patternRequest.getCourseType())
                        .build());
        return ResponseEntity.ok(new ApiResponse("success save pattern"));
    }

    public ResponseEntity executePattern(PatternRequest patternRequest) {

        String code = modifyCode(patternRequest.getPattern());
        Map<String, String> compile = cLientApi.compile(
                CodexRequest.builder()
                        .code(code)
                        .language(language)
                        .build());
        compile.put("code", code);

        return ResponseEntity.ok(new ApiResponseData<>(
                CreatePatternResponse.builder()
                        .patternResult(code)
                        .output(compile.get("output"))
                        .error(compile.get("error"))
                        .build()));
    }

    public ResponseEntity getAllPattern() {
        List<Pattern> patterns = patternRepository.findAllByOrderByIdDesc();
        return ResponseEntity.ok(new ApiResponseData<>(patterns));
    }

    public ResponseEntity getExercisePatternByExercise(Integer id) {
        return ResponseEntity.ok(exercisePatternRepository.findAllByExerciseId(id));
    }


}
