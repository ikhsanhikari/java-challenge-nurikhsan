package ist.challenge.nurikhsan.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable("Nurikhsan") //should be from jwt
                .map(item -> String.valueOf(item));
    }

}
