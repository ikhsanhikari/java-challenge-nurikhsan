package ist.challenge.nurikhsan.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ist.challenge.nurikhsan.util.Constant;

@FeignClient(value = "codeCompiler", url = Constant.CODEX_API)
public interface CLientApi {

    @PostMapping("/")
    Map<String, String> compile(@RequestBody CodexRequest codexRequest);

}
