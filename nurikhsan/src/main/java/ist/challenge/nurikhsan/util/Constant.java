package ist.challenge.nurikhsan.util;

public interface Constant {

    public final String PING_API = "/ping";
    public final String EXERCISE_ANSWER_API = "/exercise_answer";
    public final String EXERCISE_API = "/exercise";
    public final String EXERCISE_PATTERN_API = "/exercise_pattern";
    public final String PATTERN_API = "/pattern";

    public final String CODEX_API = "http://localhost:3333";

}
