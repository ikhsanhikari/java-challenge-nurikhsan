package ist.challenge.nurikhsan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.nurikhsan.payload.PatternRequest;
import ist.challenge.nurikhsan.service.PatternService;
import ist.challenge.nurikhsan.util.Constant;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = Constant.PATTERN_API)
public class PatternController {
    private final PatternService patternService;

    @Operation(summary = "Api untuk save pattern")
    @PostMapping
    public ResponseEntity createPattern(@RequestBody PatternRequest patternRequest) {
        return patternService.createPattern(patternRequest);
    }

    @Operation(summary = "Api untuk execute pattern")
    @PostMapping("/execute")
    public ResponseEntity executePattern(@RequestBody PatternRequest patternRequest) {
        return patternService.executePattern(patternRequest);
    }

    
    @Operation(summary = "Api untuk mendapatkan semua pattern")
    @GetMapping
    public ResponseEntity getAllPattern() {
        return patternService.getAllPattern();
    }

}
