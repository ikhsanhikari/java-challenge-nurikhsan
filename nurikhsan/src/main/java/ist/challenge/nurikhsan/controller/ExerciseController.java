package ist.challenge.nurikhsan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.nurikhsan.payload.ExerciseRequest;
import ist.challenge.nurikhsan.service.ExerciseService;
import ist.challenge.nurikhsan.util.Constant;
import lombok.RequiredArgsConstructor;

@RequestMapping(value = Constant.EXERCISE_API)
@RestController
@RequiredArgsConstructor
public class ExerciseController {

    private final ExerciseService exerciseService;

    @Operation(summary = "Api untuk generate random soal dari patern by exercise_id")
    @GetMapping("/{id}")
    public ResponseEntity generateExerciseFromPattern(@PathVariable("id") Integer id) {
        return exerciseService.generateExerciseFromPattern(id);
    }

    @Operation(summary = "Api untuk mendapatkan semua exercise")
    @GetMapping
    public ResponseEntity getAllExercise() {
        return exerciseService.getAllExercise();
    }

    @Operation(summary = "Api untuk save exercise berdasarkan pattern yang di pilih dan jumlah berapa kali generate per pattern")
    @PostMapping
    public ResponseEntity create(@RequestBody ExerciseRequest exerciseRequest) {
        return exerciseService.createExercise(exerciseRequest);
    }

}
