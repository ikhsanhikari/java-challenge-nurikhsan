package ist.challenge.nurikhsan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.nurikhsan.service.ExerciseService;
import ist.challenge.nurikhsan.util.Constant;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = Constant.EXERCISE_ANSWER_API)
public class ExerciseAnswerController {

    private final ExerciseService exerciseService;

    @Operation(summary = "Api untuk menampilkan exercise dan answer by generateId")
    @GetMapping("/{generateId}")
    public ResponseEntity getByGenerateId(@PathVariable("generateId") String generateId) {
        return exerciseService.getExerciseAnswerByGenerateId(generateId);
    }

}
