package ist.challenge.nurikhsan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.nurikhsan.service.PatternService;
import ist.challenge.nurikhsan.util.Constant;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = Constant.EXERCISE_PATTERN_API)
public class ExercisePatternController {
    private final PatternService patternService;

    @Operation(summary = "Api untuk mendapatkan exercise pattern by exercise id")
    @GetMapping
    public ResponseEntity getExercisePattern(@RequestParam("exerciseId") Integer exerciseId) {
        return patternService.getExercisePatternByExercise(exerciseId);
    }
}
